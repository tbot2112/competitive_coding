def primeGenerator():
    yield 2
    yield 3
    primes = [2,3]
    a = 5
    while True:
        prime = True
        for p in primes:
            if a%p == 0:
                prime = False
        if prime:
            primes.append(a)
            yield a
        a += 2

def primeFactor(n):
    factors = []
    gen = primeGenerator()
  
    while n > 1:
        p = gen.next()
        while n%p == 0:
            factors.append(p)
            n /= p
  
    return factors