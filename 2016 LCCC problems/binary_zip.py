N = int(raw_input())
a, b = map(int,raw_input().split())
a = bin(a)[2:]
b = bin(b)[2:]

if len(a) < N:
    a = '0'* (N-len(a)) + a

if len(b) < N:
    b = '0'* (N-len(b)) + b

zip = ''.join(map(str,map(int,[not int(c) for c in a]))) == b
    
if zip:
    print 'YES'
else:
    print 'NO'