##TIMES OUT ON BIGGEST TEST CASE


r,c = map(int, raw_input().split())
max_len = 0
d = {}
for i in range(r):
    chars = raw_input().split()
    for j, char in enumerate(chars):
        if char in '<>^V':
            d[(i,j)] = char
        elif char != '-':
            raise ValueError('unexpected character in the input')
            
def traverse(root):
    current = root
    visited = [root]
    
    while True:
        past = current
        if d[current] == '^':
            current = (current[0] - 1, current[1])
        elif d[current] == '>':
            current = (current[0], current[1] + 1)
        elif d[current] == 'V':
            current = (current[0] + 1, current[1])
        elif d[current] == '<':
            current = (current[0], current[1] - 1)
        else:
            raise ValueError('unexpected character in dict')
        #print 'Current location:', current, 'r =', r, 'c =', c
        
        
        if current not in d:
            return visited, 0
        if current in visited:
            return visited, len(visited) - visited.index(current)
        visited.append(current)
        
while len(d) > max_len:
    visited, loop_len = traverse(d.keys()[0])
    if loop_len > max_len:
        max_len = loop_len
    for loc in visited:
        d.pop(loc)
print max_len