import math
d = {}

def prime_generator():
    yield 2
    yield 3
    primes = set((2,3))
    i = 5
    while True:
        i_is_prime = True
        for p in primes:
            if i % p == 0:
                i_is_prime = False
                break
        if i_is_prime:
            yield i
            primes.add(i)
        i += 2
        
gen = prime_generator()
n = int(raw_input())
p = gen.next()
while n > 1:
    if n % p == 0:
        if p not in d:
            d[p] = 1
        else:
            d[p] += 1
        n /= p
    else:
        p = gen.next()
print max(d.values())