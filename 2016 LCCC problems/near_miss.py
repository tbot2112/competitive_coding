import math
xr, yr, zr = map(int, raw_input().split())
vxr, vyr, vzr = map(int, raw_input().split())
xc, yc, zc = map(int, raw_input().split())
vxc, vyc, vzc = map(int, raw_input().split())

dx = xr - xc
dy = yr - yc
dz = zr - zc
dvx = vxr - vxc
dvy = vyr - vyc
dvz = vzr - vzc

def d(t):
    return math.sqrt((dx + t*dvx)**2 + (dy + t*dvy)**2 + (dz + t*dvz)**2)

if (dvx and dvy and dvz) == 0:
    print d(0)
else:
    tstar = -(1.*dx*dvx + dy*dvy + dz*dvz)/(dvx**2 + dvy**2 + dvz**2)

    if tstar > 0:
        print d(tstar)
    else:
        print d(0)