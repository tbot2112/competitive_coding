from math import log

k = long(raw_input())
n = long(raw_input())

if n == 1 and k == 1:
    print 'YES'
elif n == 1:
    print 'NO'
elif n == 0:
    print 'NO'
else:
    i = log(k)/log(n)

    #if abs(int(i) - i) <= 0.0000000001:
    if abs(int(i) - i) <= 0.0001:
        print 'YES'
    else:
        print 'NO'