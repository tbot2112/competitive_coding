from euler_helper import primeGenerator

def sum_of_primes_sieved(top = 2e6):
    l = sieved_list(top)
    return sum(l)
    
def sieved_list(top):
    l = range(2,top)
    primes = [2]
    g = primeGenerator()
    p = g.next()
    while p*p <= top:
        i = 1
        while i*p < top:
            if i*p in l:
                l.remove(i*p)
            i += 1
        p = g.next()
        primes.append(p)
    return primes + l 
    
print sum_of_primes_sieved()